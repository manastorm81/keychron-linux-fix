# keychron_linux_fix_debian.sh
# This script fixes the F1-F12 keys for keychron keyboards on linux.

echo ---- START OF SCRIPT ----
echo ---- Fixing F1-F12 for current session ----
echo 0 | sudo tee /sys/module/hid_apple/parameters/fnmode
echo DONE...
echo ---- Applying fix and updating initramfs ----
echo 'options hid_apple fnmode=0' | sudo tee /etc/modprobe.d/hid_apple.conf 
sudo update-initramfs -u
echo ---- End of script ----
