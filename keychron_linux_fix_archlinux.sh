# keychron_linux_fix_arch.sh
# This script fixes the F1-F12 keys for keychron keyboards on linux.

echo Fixing F1-F12 for current session...
echo 0 | sudo tee /sys/module/hid_apple/parameters/fnmode
echo DONE
echo Starting fix...
echo 'options hid_apple fnmode=0' | sudo tee /etc/modprobe.d/hid_apple.conf 
echo Find the linux vesion number in next line.
ls -l /etc/mkinitcpio.d
echo Use the following command to update the system and replace XYZ with the linux version number.
echo sudo mkinitcpio -p linuxXYZ
